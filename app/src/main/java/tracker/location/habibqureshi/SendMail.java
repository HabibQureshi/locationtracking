package tracker.location.habibqureshi;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class SendMail extends AsyncTask<String, Void, Boolean> {
    private ProgressDialog progress;
    private Context context;


    SendMail(Context c, ProgressDialog progress){
        this.progress=progress;
        this.context=c;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        this.progress.setMessage("Sending Email...");
    }

    @Override
    protected Boolean doInBackground(String... urls) {
        try {
            GMailSender sender = new GMailSender("habibsaquib1995@gmail.com", "habibqureshi2478");
            sender.sendMail("User Location",
                    "here is the user location : "+urls[0],
                    "habibsaquib1995@gmail.com",
                    "denyss@gmail.com");
            return true;

        } catch (Exception e) {
            Log.e("SendMail", e.getMessage(), e);
           return false;

        }
    }


    @Override
    protected void onProgressUpdate(Void... progress) {

    }
    @Override
    protected void onPostExecute(Boolean result) {
        progress.cancel();
        if(result)
            this.displayMessage("Email sent....");
        else
            this.displayMessage("Error in sending email");



    }

    private void displayMessage(String message){
        Toast.makeText(this.context,
                message,
                Toast.LENGTH_SHORT).show();
    }
}