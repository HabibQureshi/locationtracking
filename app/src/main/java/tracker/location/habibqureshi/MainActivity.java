package tracker.location.habibqureshi;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class MainActivity extends AppCompatActivity implements LocationListener,  OnCompleteListener<Location>{

    Button location;
    LocationManager manager;
    public static boolean isConnected = false;
    private LocationRequest mLocationRequest;
    private static int UPDATE_INTERVAL = 10000;
    private static int FASTEST_INTERVAL = 5000;
    private static int DISPLACEMENT = 10;
    private static final String TAG = MainActivity.class.getSimpleName();
    LocationManager locationManager;
    private Location currentLocation;
    private Task<Location> mLastLocation;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private ProgressDialog progress;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        location = findViewById(R.id.location);
        manager = (LocationManager) this.getSystemService(this.LOCATION_SERVICE);
        progress=new ProgressDialog(this);

        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLocation(MainActivity.this);

            }
        });
    }

    public boolean checkLocationAppPermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    public boolean isGPSEnable() {
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }


    @SuppressLint("MissingPermission")
    public void displayLocationSettingsRequest() {
        M.l("request for gps");
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }




    public void getLocation(OnCompleteListener<Location> listener) {
        if (this.checkLocationAppPermission()) {
            if (this.isGPSEnable()) {
                progress.setMessage("Getting your location");
                progress.show();
                mLastLocation = fusedLocationProviderClient.getLastLocation();
                mLastLocation.addOnCompleteListener(listener);

            } else
                displayLocationSettingsRequest();
        } else this.requestLocationAppPermission();

    }

    @Override
    public void onComplete(@NonNull Task<Location> task) {
        this.currentLocation=task.getResult();
        if(currentLocation!=null){
            sendMyLocation(this.currentLocation);
        }
        else {
            Toast.makeText(MainActivity.this,"Failed to get location",Toast.LENGTH_LONG).show();
            progress.cancel();
        }

    }

    private void sendMyLocation(Location currentLocation) {

        String uri = "http://maps.google.com/maps?saddr=" + currentLocation.getLatitude() + "," + currentLocation.getLongitude();
        M.l(uri);
        new SendMail(this,progress).execute(uri);


    }

    public void requestLocationAppPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                99);
    }

    @Override
    public void onLocationChanged(Location location) {
        this.currentLocation=location;
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

}

